window.onload = function(){

  var submit = [
    document.getElementById('dashboard_form_submit_1'),
    document.getElementById('dashboard_form_submit_2'),
    document.getElementById('dashboard_form_submit_3'),
    document.getElementById('dashboard_form_submit_4'),
    document.getElementById('dashboard_form_submit_5'),
    document.getElementById('dashboard_form_submit_6'),
    document.getElementById('dashboard_clear_button_1'),
    document.getElementById('dashboard_clear_button_2'),
    document.getElementById('dashboard_clear_button_3'),
    document.getElementById('dashboard_clear_button_4'),
    document.getElementById('dashboard_clear_button_5'),
    document.getElementById('dashboard_clear_button_6'),
  ];

  var dashboard_form = [
    document.getElementById('dashboard_form_1'),
    document.getElementById('dashboard_form_2'),
    document.getElementById('dashboard_form_3'),
    document.getElementById('dashboard_form_4'),
    document.getElementById('dashboard_form_5'),
    document.getElementById('dashboard_form_6'),
  ];

  var dashboard_clear = [
    document.getElementById('dashboard_clear_1'),
    document.getElementById('dashboard_clear_2'),
    document.getElementById('dashboard_clear_3'),
    document.getElementById('dashboard_clear_4'),
    document.getElementById('dashboard_clear_5'),
    document.getElementById('dashboard_clear_6'),
  ];

  var dashboard_overzicht = document.getElementById('dashboard_overzicht');

  var optie = [
    document.getElementById('optie_1'),
    document.getElementById('optie_2'),
    document.getElementById('optie_3'),
    document.getElementById('optie_4'),
    document.getElementById('optie_5'),
    document.getElementById('optie_6'),
  ];

  var balk = [
    document.getElementById('balk_intern_1'),
    document.getElementById('balk_intern_2'),
    document.getElementById('balk_intern_3'),
    document.getElementById('balk_intern_4'),
    document.getElementById('balk_intern_5'),
    document.getElementById('balk_intern_6')
  ];

  var balk_span = [
    document.getElementById('balk_span_1'),
    document.getElementById('balk_span_2'),
    document.getElementById('balk_span_3'),
    document.getElementById('balk_span_4'),
    document.getElementById('balk_span_5'),
    document.getElementById('balk_span_6'),
  ];

  var balk_span_percentage = [
    document.getElementById('balk_span_percentage_1'),
    document.getElementById('balk_span_percentage_2'),
    document.getElementById('balk_span_percentage_3'),
    document.getElementById('balk_span_percentage_4'),
    document.getElementById('balk_span_percentage_5'),
    document.getElementById('balk_span_percentage_6'),
  ];

  var submit_confirm_box_background = document.getElementById('submit_confirm_box_background');
  var submit_confirm_box = document.getElementById('submit_confirm_box');
  var submit_confirm_text = document.getElementById('submit_confirm_text');
  var submit_confirm_ja = document.getElementById('submit_confirm_ja');
  var submit_confirm_nee = document.getElementById('submit_confirm_nee');

  var alert = [
    document.getElementById('alert_1'),
    document.getElementById('alert_2'),
    document.getElementById('alert_3'),
    document.getElementById('alert_4'),
    document.getElementById('alert_5'),
    document.getElementById('alert_6'),
    document.getElementById('alert_7'),
  ];

  var alert_button = [
    document.getElementById('alert_button_1'),
    document.getElementById('alert_button_2'),
    document.getElementById('alert_button_3'),
    document.getElementById('alert_button_4'),
    document.getElementById('alert_button_5'),
    document.getElementById('alert_button_6'),
    document.getElementById('alert_button_7'),
  ];

  var alert_text = document.getElementById('alert_text');
  var pagina_selector = document.getElementById('dashboard_pagina_select');

  for(let i = 0; i < 6; i++){
    if(window.location.pathname == "/pagina_1" && alert_text){
      alert_button[0].onclick = function(){
        alert[0].style.display = 'none';
      };
    } else if(window.location.pathname == "/pagina_2" && alert_text){
      alert_button[1].onclick = function(){
        alert[1].style.display = 'none';
      };
    } else if(window.location.pathname == "/pagina_3" && alert_text){
      alert_button[2].onclick = function(){
        alert[2].style.display = 'none';
      };
    } else if(window.location.pathname == "/pagina_4" && alert_text){
      alert_button[3].onclick = function(){
        alert[3].style.display = 'none';
      };
    } else if(window.location.pathname == "/pagina_5" && alert_text){
      alert_button[4].onclick = function(){
        alert[4].style.display = 'none';
      };
    } else if(window.location.pathname == "/pagina_6" && alert_text){
      alert_button[5].onclick = function(){
        alert[5].style.display = 'none';
      };
    }
  }

  if(window.location.pathname == "/dashboard"){

    for(let i = 0; i < submit.length; i++){
      submit[i].onclick = function(){
        submit_confirm(i);
      };
    }

    function submit_confirm(pagina_nummer){
      if(pagina_nummer < 6){
        submit_confirm_text.innerHTML = 'Weet u zeker dat u deze aanpassingen van pagina ' + (pagina_nummer+1) + ' wilt uploaden?';
      } else if(pagina_nummer > 5){
        submit_confirm_text.innerHTML = 'Weet u zeker dat u de likes van deze pagina wilt resetten?';
      }
      submit_confirm_box.style.display = 'block';
      submit_confirm_box_background.style.display = 'block';
      submit_confirm_ja.onclick = function(){
        submit_confirm_box.style.display = 'none';
        submit_confirm_box_background.style.display = 'none';
        if(pagina_nummer < 6){
          dashboard_form[pagina_nummer].submit();
        } else if(pagina_nummer > 5){
          dashboard_clear[pagina_nummer-6].submit();
        }
      };
      submit_confirm_nee.onclick = function(){
        submit_confirm_box.style.display = 'none';
        submit_confirm_box_background.style.display = 'none';
      };
    }

    function hide_forms(){
      for(let i = 0; i<dashboard_form.length; i++){
        dashboard_form[i].style.display = "none";
      }
      dashboard_overzicht.style.display = "none";
    }

    function select_form(){
      for(let i = 0; i<7; i++){
        if(pagina_selector.options[pagina_selector.selectedIndex].value == i && i <6){
          hide_forms();
          dashboard_form[i].style.display = "block";
        } else if(pagina_selector.options[pagina_selector.selectedIndex].value == 6){
          hide_forms();
          dashboard_overzicht.style.display = "block";
        }
      }
    }

    pagina_selector.onchange = function(){
      select_form();
    };

    for(let i = 0; i < balk_span.length; i++){
      var lengtes = balk_span[i].innerHTML.split("/");
      parseFloat(lengtes[0]);
      parseFloat(lengtes[1]);
      var percentage = Math.round((lengtes[0]/lengtes[1]*100));
      balk[i].style.width = percentage + "%";
      if(!Number.isInteger(percentage)){
        balk_span_percentage[i].innerHTML = "Geen likes";
      } else{
        balk_span_percentage[i].innerHTML = percentage + "%";
      }
    }

    alert_button[6].onclick = function(){
      alert[6].style.display = 'none';
    };

    if(alert_text.innerHTML.includes("succesvol") || alert_text.innerHTML.includes("gereset")){
      alert[6].classList.add("alert_succes");
      alert_button[6].classList.add("alert_button_succes");
    }
  }
};
