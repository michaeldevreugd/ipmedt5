<?php

use Illuminate\Database\Seeder;

class PaginasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('paginas')->insert([
        'pagina'=>'pagina_1',
      ]);
      DB::table('paginas')->insert([
        'pagina'=>'pagina_2',
      ]);
      DB::table('paginas')->insert([
        'pagina'=>'pagina_3',
      ]);
      DB::table('paginas')->insert([
        'pagina'=>'pagina_4',
      ]);
      DB::table('paginas')->insert([
        'pagina'=>'pagina_5',
      ]);
      DB::table('paginas')->insert([
        'pagina'=>'pagina_6',
      ]);
    }
}
