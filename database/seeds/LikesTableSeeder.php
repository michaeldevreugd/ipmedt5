<?php

use Illuminate\Database\Seeder;

class LikesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('likes')->insert([
        'pagina'=>'pagina_1',
        'nuttig'=>0,
        'totaal'=>0
      ]);
      DB::table('likes')->insert([
        'pagina'=>'pagina_2',
        'nuttig'=>0,
        'totaal'=>0
      ]);
      DB::table('likes')->insert([
        'pagina'=>'pagina_3',
        'nuttig'=>0,
        'totaal'=>0
      ]);
      DB::table('likes')->insert([
        'pagina'=>'pagina_4',
        'nuttig'=>0,
        'totaal'=>0
      ]);
      DB::table('likes')->insert([
        'pagina'=>'pagina_5',
        'nuttig'=>0,
        'totaal'=>0
      ]);
      DB::table('likes')->insert([
        'pagina'=>'pagina_6',
        'nuttig'=>0,
        'totaal'=>0
      ]);
    }
}
