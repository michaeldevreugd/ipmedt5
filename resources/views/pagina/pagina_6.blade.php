<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ins Blau</title>
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</head>
<body class="text">
  @if(session()->has('message'))
  <div id="alert_6" class="alert">
      <span id="alert_text" class="alert_text">{{ session()->get('message') }}</span>
      <div class="alert_button_wrapper">
        <button id="alert_button_6" class="alert_button">Ok</button>
      </div>
  </div>
  @endif
  @if($pagina)
  <div id="textvak">
      <img id="logoText" src="{{asset('img/logo.png')}}" alt="">
      <h1 id="titel">{{$pagina->titel}}</h1>
      <div id="verhaal">
          <img id="image" src="{{asset('storage/image_6')}}" alt="">
          <pre>{{$pagina->tekst}}</pre>
      </div>
  </div>
  @endif
  <div id="likeDislikeWrapper">
    <div id="likeDislike">
      <form class="likeDislikeForm" action="/pagina_6" method="post">
        {{ csrf_field() }}
        <input class="hidden" type="radio" name="ja" value="ja" checked="checked">
        <button class="likeDislikeButtons" type="submit"><img class="likeDislikeImage" src="{{asset('img/like.png')}}" alt=""></button>
      </form>
      <form class="likeDislikeForm" action="/pagina_6" method="post">
        {{ csrf_field() }}
        <input class="hidden" type="radio" name="nee" value="nee" checked="checked">
        <button class="likeDislikeButtons" type="submit"><img class="likeDislikeImage" src="{{asset('img/dislike.png')}}" alt=""></button>
      </form>
    </div>
  </div>
</body>
</html>
