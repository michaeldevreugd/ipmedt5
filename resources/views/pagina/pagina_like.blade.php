<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ins Blau</title>
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>
<body class="text">
  @if($likes)
  <h1 class="likeText">{{$likes->nuttig}} van de {{$likes->totaal}} mensen vonden dit leuk</h1>
  @endif
  <div class="terug_knop_wrapper">
    <a href="javascript:history.back()" class="terug_knop">Terug</a>
  </div>
</body>
</html>
