<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ins Blau | Dashboard</title>
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <script type="text/javascript" src="{{asset('js/main.js')}}"></script>
  </head>
  <body class="text">
    <div class="content">
      <select id="dashboard_pagina_select" class="dashboard_pagina_select">
        <option id="optie_1" value="0">Pagina 1</option>
        <option id="optie_2" value="1">Pagina 2</option>
        <option id="optie_3" value="2">Pagina 3</option>
        <option id="optie_4" value="3">Pagina 4</option>
        <option id="optie_5" value="4">Pagina 5</option>
        <option id="optie_6" value="5">Pagina 6</option>
        <option id="optie_7" value="6">Overzicht</option>
      </select>

      @if(session()->has('message'))
      <div id="alert_7" class="alert">
          <span id="alert_text" class="alert_text">{{ session()->get('message') }}</span>
          <div class="alert_button_wrapper">
            <button id="alert_button_7" class="alert_button">Ok</button>
          </div>
      </div>
      @endif

      <form enctype="multipart/form-data" id="dashboard_form_1" name="dashboard_form_1" class="dashboard_form" action="/dashboard" method="post">
        {{ csrf_field() }}
        <h1 class="dashboard_pagina">Pagina 1</h1>
        <label for="pagina_1_titel">Titel</label>
        <input id="pagina_1_titel" type="text" name="pagina_1_titel" value="">

        <label for="pagina_1_img">Afbeelding (.png)</label>
        <input id="pagina_1_img" type="file" accept=".png" name="pagina_1_img">

        <label for="pagina_1_tekst">Tekst</label>
        <textarea id="pagina_1_tekst" name="pagina_1_tekst" class="text_input" rows="8" cols="80"></textarea>

        <div class="dashboard_upload_wrapper">
          <button id="dashboard_form_submit_1" class="text_input_button" type="button">Upload</button>
        </div>
        <input class="hidden" type="radio" name="pagina_1" value="pagina_1" checked="checked">
      </form>

      <form enctype="multipart/form-data" id="dashboard_form_2" name="dashboard_form_2" class="dashboard_form hidden" action="/dashboard" method="post">
        {{ csrf_field() }}
        <h1 class="dashboard_pagina">Pagina 2</h1>
        <label for="pagina_2_titel">Titel</label>
        <input id="pagina_2_titel" type="text" name="pagina_2_titel" value="">

        <label for="pagina_2_img">Afbeelding (.png)</label>
        <input id="pagina_2_img" type="file" accept=".png" name="pagina_2_img">

        <label for="pagina_2_tekst">Tekst</label>
        <textarea id="pagina_2_tekst" name="pagina_2_tekst" class="text_input" rows="8" cols="80"></textarea>

        <div class="dashboard_upload_wrapper">
          <button id="dashboard_form_submit_2" class="text_input_button" type="button">Upload</button>
        </div>
        <input class="hidden" type="radio" name="pagina_2" value="pagina_2" checked="checked">
      </form>

      <form enctype="multipart/form-data" id="dashboard_form_3" name="dashboard_form_3" class="dashboard_form hidden" action="/dashboard" method="post">
        {{ csrf_field() }}
        <h1 class="dashboard_pagina">Pagina 3</h1>
        <label for="pagina_3_titel">Titel</label>
        <input id="pagina_3_titel" type="text" name="pagina_3_titel" value="">

        <label for="pagina_3_img">Afbeelding (.png)</label>
        <input id="pagina_3_img" type="file" accept=".png" name="pagina_3_img">

        <label for="pagina_3_tekst">Tekst</label>
        <textarea id="pagina_3_tekst" name="pagina_3_tekst" class="text_input" rows="8" cols="80"></textarea>

        <div class="dashboard_upload_wrapper">
          <button id="dashboard_form_submit_3" class="text_input_button" type="button">Upload</button>
        </div>
        <input class="hidden" type="radio" name="pagina_3" value="pagina_3" checked="checked">
      </form>

      <form enctype="multipart/form-data" id="dashboard_form_4" name="dashboard_form_4" class="dashboard_form hidden" action="/dashboard" method="post">
        {{ csrf_field() }}
        <h1 class="dashboard_pagina">Pagina 4</h1>
        <label for="pagina_4_titel">Titel</label>
        <input id="pagina_4_titel" type="text" name="pagina_4_titel" value="">

        <label for="pagina_4_img">Afbeelding (.png)</label>
        <input id="pagina_4_img" type="file" accept=".png" name="pagina_4_img">

        <label for="pagina_4_tekst">Tekst</label>
        <textarea id="pagina_4_tekst" name="pagina_4_tekst" class="text_input" rows="8" cols="80"></textarea>

        <div class="dashboard_upload_wrapper">
          <button id="dashboard_form_submit_4" class="text_input_button" type="button">Upload</button>
        </div>
        <input class="hidden" type="radio" name="pagina_4" value="pagina_4" checked="checked">
      </form>

      <form enctype="multipart/form-data" id="dashboard_form_5" name="dashboard_form_5" class="dashboard_form hidden" action="/dashboard" method="post">
        {{ csrf_field() }}
        <h1 class="dashboard_pagina">Pagina 5</h1>
        <label for="pagina_5_titel">Titel</label>
        <input id="pagina_5_titel" type="text" name="pagina_5_titel">

        <label for="pagina_5_img">Afbeelding (.png)</label>
        <input id="pagina_5_img" type="file" accept=".png" name="pagina_5_img">

        <label for="pagina_5_tekst">Tekst</label>
        <textarea id="pagina_5_tekst" name="pagina_5_tekst" class="text_input" rows="8" cols="80"></textarea>

        <div class="dashboard_upload_wrapper">
          <button id="dashboard_form_submit_5" class="text_input_button" type="button">Upload</button>
        </div>
        <input class="hidden" type="radio" name="pagina_5" value="pagina_5" checked="checked">
      </form>

      <form enctype="multipart/form-data" id="dashboard_form_6" name="dashboard_form_6" class="dashboard_form hidden" action="/dashboard" method="post">
        {{ csrf_field() }}
        <h1 class="dashboard_pagina">Pagina 6</h1>
        <label for="pagina_6_titel">Titel</label>
        <input id="pagina_6_titel" type="text" name="pagina_6_titel">

        <label for="pagina_6_img">Afbeelding (.png)</label>
        <input id="pagina_6_img" type="file" accept=".png" name="pagina_6_img">

        <label for="pagina_6_tekst">Tekst</label>
        <textarea id="pagina_6_tekst" name="pagina_6_tekst" class="text_input" rows="8" cols="80"></textarea>

        <div class="dashboard_upload_wrapper">
          <button id="dashboard_form_submit_6" class="text_input_button" type="button">Upload</button>
        </div>
        <input class="hidden" type="radio" name="pagina_6" value="pagina_6" checked="checked">
      </form>

      <div id="dashboard_overzicht" class="dashboard_overzicht hidden">
        @if($likes)
        <h2 class="balk_titel">
          @if($inhoud)
            {{$inhoud[0]->titel}} <small>({{$likes[0]->nuttig}}/{{$likes[0]->totaal}})</small>
          @endif
        </h2>
        <div class="dashboard_balk_wrapper">
          <div class="float_left">
            <div class="dashboard_overzicht_balk_extern">
              <div id="balk_intern_1" class="dashboard_overzicht_balk_intern"></div>
              <span id="balk_span_1" class="hidden">{{$likes[0]->nuttig}}/{{$likes[0]->totaal}}</span>
              <span id="balk_span_percentage_1" class="dashboard_overzicht_balk_span"></span>
            </div>
          </div>
          <div class="float_right">
            <form enctype="multipart/form-data" id="dashboard_clear_1" name="dashboard_clear_1" action="/dashboard" method="post">
              {{ csrf_field() }}
              <input class="hidden" type="radio" name="clear_1" value="clear_1" checked="checked">
              <button type="button" id="dashboard_clear_button_1" class="dashboard_likes_clear">Clear</button>
            </form>
          </div>
        </div>

        <h2 class="balk_titel">
          @if($inhoud)
            {{$inhoud[1]->titel}} <small>({{$likes[1]->nuttig}}/{{$likes[1]->totaal}})</small>
          @endif
        </h2>
        <div class="dashboard_balk_wrapper">
          <div class="float_left">
            <div class="dashboard_overzicht_balk_extern">
              <div id="balk_intern_2" class="dashboard_overzicht_balk_intern"></div>
              <span id="balk_span_2" class="hidden">{{$likes[1]->nuttig}}/{{$likes[1]->totaal}}</span>
              <span id="balk_span_percentage_2" class="dashboard_overzicht_balk_span"></span>
            </div>
          </div>
          <div class="float_right">
            <form enctype="multipart/form-data" id="dashboard_clear_2" name="dashboard_clear_2" action="/dashboard" method="post">
              {{ csrf_field() }}
              <input class="hidden" type="radio" name="clear_2" value="clear_2" checked="checked">
              <button type="button" id="dashboard_clear_button_2" class="dashboard_likes_clear">Clear</button>
            </form>
          </div>
        </div>

        <h2 class="balk_titel">
          @if($inhoud)
            {{$inhoud[2]->titel}} <small>({{$likes[2]->nuttig}}/{{$likes[2]->totaal}})</small>
          @endif
        </h2>
        <div class="dashboard_balk_wrapper">
          <div class="float_left">
            <div class="dashboard_overzicht_balk_extern">
              <div id="balk_intern_3" class="dashboard_overzicht_balk_intern"></div>
              <span id="balk_span_3" class="hidden">{{$likes[2]->nuttig}}/{{$likes[2]->totaal}}</span>
              <span id="balk_span_percentage_3" class="dashboard_overzicht_balk_span"></span>
            </div>
          </div>
          <div class="float_right">
            <form enctype="multipart/form-data" id="dashboard_clear_3" name="dashboard_clear_3" action="/dashboard" method="post">
              {{ csrf_field() }}
              <input class="hidden" type="radio" name="clear_3" value="clear_3" checked="checked">
              <button type="button" id="dashboard_clear_button_3" class="dashboard_likes_clear">Clear</button>
            </form>
          </div>
        </div>

        <h2 class="balk_titel">
          @if($inhoud)
            {{$inhoud[3]->titel}} <small>({{$likes[3]->nuttig}}/{{$likes[3]->totaal}})</small>
          @endif
        </h2>
        <div class="dashboard_balk_wrapper">
          <div class="float_left">
            <div class="dashboard_overzicht_balk_extern">
              <div id="balk_intern_4" class="dashboard_overzicht_balk_intern"></div>
              <span id="balk_span_4" class="hidden">{{$likes[3]->nuttig}}/{{$likes[3]->totaal}}</span>
              <span id="balk_span_percentage_4" class="dashboard_overzicht_balk_span"></span>
            </div>
          </div>
          <div class="float_right">
            <form enctype="multipart/form-data" id="dashboard_clear_4" name="dashboard_clear_4" action="/dashboard" method="post">
              {{ csrf_field() }}
              <input class="hidden" type="radio" name="clear_4" value="clear_4" checked="checked">
              <button type="button" id="dashboard_clear_button_4" class="dashboard_likes_clear">Clear</button>
            </form>
          </div>
        </div>

        <h2 class="balk_titel">
          @if($inhoud)
            {{$inhoud[4]->titel}} <small>({{$likes[4]->nuttig}}/{{$likes[4]->totaal}})</small>
          @endif
        </h2>
        <div class="dashboard_balk_wrapper">
          <div class="float_left">
            <div class="dashboard_overzicht_balk_extern">
              <div id="balk_intern_5" class="dashboard_overzicht_balk_intern"></div>
              <span id="balk_span_5" class="hidden">{{$likes[4]->nuttig}}/{{$likes[4]->totaal}}</span>
              <span id="balk_span_percentage_5" class="dashboard_overzicht_balk_span"></span>
            </div>
          </div>
          <div class="float_right">
            <form enctype="multipart/form-data" id="dashboard_clear_5" name="dashboard_clear_5" action="/dashboard" method="post">
              {{ csrf_field() }}
              <input class="hidden" type="radio" name="clear_5" value="clear_5" checked="checked">
              <button type="button" id="dashboard_clear_button_5" class="dashboard_likes_clear">Clear</button>
            </form>
          </div>
        </div>

        <h2 class="balk_titel">
          @if($inhoud)
            {{$inhoud[5]->titel}} <small>({{$likes[5]->nuttig}}/{{$likes[5]->totaal}})</small>
          @endif
        </h2>
        <div class="dashboard_balk_wrapper">
          <div class="float_left">
            <div class="dashboard_overzicht_balk_extern">
              <div id="balk_intern_6" class="dashboard_overzicht_balk_intern"></div>
              <span id="balk_span_6" class="hidden">{{$likes[5]->nuttig}}/{{$likes[5]->totaal}}</span>
              <span id="balk_span_percentage_6" class="dashboard_overzicht_balk_span"></span>
            </div>
          </div>
          <div class="float_right">
            <form enctype="multipart/form-data" id="dashboard_clear_6" name="dashboard_clear_6" action="/dashboard" method="post">
              {{ csrf_field() }}
              <input class="hidden" type="radio" name="clear_6" value="clear_6" checked="checked">
              <button type="button" id="dashboard_clear_button_6" class="dashboard_likes_clear">Clear</button>
            </form>
          </div>
        </div>
        @endif
      </div>
    </div>
    <div class="submit_confirm_box hidden" id="submit_confirm_box">
      <p id="submit_confirm_text"></p>
      <div class="confirm_box_button_wrapper">
        <button class="confirm_box_button" id="submit_confirm_ja" type="button" name="button">ja</button>
        <button class="confirm_box_button" id="submit_confirm_nee" type="button" name="button">nee</button>
      </div>
    </div>
    <div class="submit_confirm_box_background hidden" id="submit_confirm_box_background"></div>
  </body>
</html>
