<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\PaginaInhoud;
use App\PaginaLikes;
use Cookie;
use Redirect;

class DashboardController extends Controller
{

  public function dashboard(){
    return view('dashboard.dashboard')->with('likes',PaginaLikes::all())->with('inhoud',PaginaInhoud::all());
  }

  public function dashboard_upload(Request $request){
    if($request->has('pagina_1')){
      $tekst = $request->pagina_1_tekst;
      $titel = $request->pagina_1_titel;
      $img = $request->pagina_1_img;
      // Hier wordt gecheckt of de update van een pagina geen lege velden bevat. Als dit wel het geval is wordt er niets naar de database geschreven
      if($tekst){
        PaginaInhoud::where('pagina', '=', 'pagina_1')->update(['tekst' => $tekst]);
      }
      if($titel){
        PaginaInhoud::where('pagina', '=', 'pagina_1')->update(['titel' => $titel]);
      }
      if($img){
        $path = Storage::putFileAs('public', $img, 'image_1');
      }
      // Als alle velden in het dashboard leeg zijn en er wordt geprobeerd te updaten, dan wordt er een melding getoond die er op wijst dat er niets ingevuld is
      if(!$img && !$titel && !$tekst){
        return Redirect::to('/dashboard')->with('message', 'U heeft niets ingevuld, pagina 1 is niet geüpdatet.');
      } else{
        return Redirect::to('/dashboard')->with('message', 'Pagina 1 is succesvol geüpdatet.');
      }
    }

    if($request->has('pagina_2')){
      $tekst = $request->pagina_2_tekst;
      $titel = $request->pagina_2_titel;
      $img = $request->pagina_2_img;
      if($tekst){
        PaginaInhoud::where('pagina', '=', 'pagina_2')->update(['tekst' => $tekst]);
      }
      if($titel){
        PaginaInhoud::where('pagina', '=', 'pagina_2')->update(['titel' => $titel]);
      }
      if($img){
        $path = Storage::putFileAs('public', $img, 'image_2');
      }
      if(!$img && !$titel && !$tekst){
        return Redirect::to('/dashboard')->with('message', 'U heeft niets ingevuld, pagina 2 is niet geüpdatet.');
      } else{
        return Redirect::to('/dashboard')->with('message', 'Pagina 2 is succesvol geüpdatet.');
      }
    }

    if($request->has('pagina_3')){
      $tekst = $request->pagina_3_tekst;
      $titel = $request->pagina_3_titel;
      $img = $request->pagina_3_img;
      if($tekst){
        PaginaInhoud::where('pagina', '=', 'pagina_3')->update(['tekst' => $tekst]);
      }
      if($titel){
        PaginaInhoud::where('pagina', '=', 'pagina_3')->update(['titel' => $titel]);
      }
      if($img){
        $path = Storage::putFileAs('public', $img, 'image_3');
      }
      if(!$img && !$titel && !$tekst){
        return Redirect::to('/dashboard')->with('message', 'U heeft niets ingevuld, pagina 3 is niet geüpdatet.');
      } else{
        return Redirect::to('/dashboard')->with('message', 'Pagina 3 is succesvol geüpdatet');
      }
    }

    if($request->has('pagina_4')){
      $tekst = $request->pagina_4_tekst;
      $titel = $request->pagina_4_titel;
      $img = $request->pagina_4_img;
      if($tekst){
        PaginaInhoud::where('pagina', '=', 'pagina_4')->update(['tekst' => $tekst]);
      }
      if($titel){
        PaginaInhoud::where('pagina', '=', 'pagina_4')->update(['titel' => $titel]);
      }
      if($img){
        $path = Storage::putFileAs('public', $img, 'image_4');
      }
      if(!$img && !$titel && !$tekst){
        return Redirect::to('/dashboard')->with('message', 'U heeft niets ingevuld, pagina 4 is niet geüpdatet.');
      } else{
        return Redirect::to('/dashboard')->with('message', 'Pagina 4 is succesvol geüpdatet.');
      }
    }

    if($request->has('pagina_5')){
      $tekst = $request->pagina_5_tekst;
      $titel = $request->pagina_5_titel;
      $img = $request->pagina_5_img;
      if($tekst){
        PaginaInhoud::where('pagina', '=', 'pagina_5')->update(['tekst' => $tekst]);
      }
      if($titel){
        PaginaInhoud::where('pagina', '=', 'pagina_5')->update(['titel' => $titel]);
      }
      if($img){
        $path = Storage::putFileAs('public', $img, 'image_5');
      }
      if(!$img && !$titel && !$tekst){
        return Redirect::to('/dashboard')->with('message', 'U heeft niets ingevuld, pagina 5 is niet geüpdatet.');
      } else{
        return Redirect::to('/dashboard')->with('message', 'Pagina 5 is succesvol geüpdatet.');
      }
    }

    if($request->has('pagina_6')){
      $tekst = $request->pagina_6_tekst;
      $titel = $request->pagina_6_titel;
      $img = $request->pagina_6_img;
      if($tekst){
        PaginaInhoud::where('pagina', '=', 'pagina_6')->update(['tekst' => $tekst]);
      }
      if($titel){
        PaginaInhoud::where('pagina', '=', 'pagina_6')->update(['titel' => $titel]);
      }
      if($img){
        $path = Storage::putFileAs('public', $img, 'image_6');
      }
      if(!$img && !$titel && !$tekst){
        return Redirect::to('/dashboard')->with('message', 'U heeft niets ingevuld, pagina 6 is niet geüpdatet.');
      } else{
        return Redirect::to('/dashboard')->with('message', 'Pagina 6 is succesvol geüpdatet.');
      }
    }

    if($request->has('clear_1')){
      PaginaLikes::where('pagina', '=', 'pagina_1')->update(['nuttig' => 0, 'totaal' => 0]);
      return Redirect::to('/dashboard')->with('message', 'De likes van pagina 1 zijn gereset.');
    }

    if($request->has('clear_2')){
      PaginaLikes::where('pagina', '=', 'pagina_2')->update(['nuttig' => 0, 'totaal' => 0]);
      return Redirect::to('/dashboard')->with('message', 'De likes van pagina 2 zijn gereset.');
    }

    if($request->has('clear_3')){
      PaginaLikes::where('pagina', '=', 'pagina_3')->update(['nuttig' => 0, 'totaal' => 0]);
      return Redirect::to('/dashboard')->with('message', 'De likes van pagina 3 zijn gereset.');
    }

    if($request->has('clear_4')){
      PaginaLikes::where('pagina', '=', 'pagina_4')->update(['nuttig' => 0, 'totaal' => 0]);
      return Redirect::to('/dashboard')->with('message', 'De likes van pagina 4 zijn gereset.');
    }

    if($request->has('clear_5')){
      PaginaLikes::where('pagina', '=', 'pagina_5')->update(['nuttig' => 0, 'totaal' => 0]);
      return Redirect::to('/dashboard')->with('message', 'De likes van pagina 5 zijn gereset.');
    }

    if($request->has('clear_6')){
      PaginaLikes::where('pagina', '=', 'pagina_6')->update(['nuttig' => 0, 'totaal' => 0]);
      return Redirect::to('/dashboard')->with('message', 'De likes van pagina 6 zijn gereset.');
    }
  }
}
