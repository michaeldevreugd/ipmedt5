<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\PaginaInhoud;
use App\PaginaLikes;
use Cookie;
use Redirect;
class PaginaController extends Controller
{
    public function pagina_1(){
      return view('pagina.pagina_1')->with('pagina',PaginaInhoud::where('pagina', '=', 'pagina_1')->get()->first());
    }

    public function pagina_2(){
      return view('pagina.pagina_2')->with('pagina',PaginaInhoud::where('pagina', '=', 'pagina_2')->get()->first());
    }

    public function pagina_3(){
      return view('pagina.pagina_3')->with('pagina',PaginaInhoud::where('pagina', '=', 'pagina_3')->get()->first());
    }

    public function pagina_4(){
      return view('pagina.pagina_4')->with('pagina',PaginaInhoud::where('pagina', '=', 'pagina_4')->get()->first());
    }

    public function pagina_5(){
      return view('pagina.pagina_5')->with('pagina',PaginaInhoud::where('pagina', '=', 'pagina_5')->get()->first());
    }

    public function pagina_6(){
      return view('pagina.pagina_6')->with('pagina',PaginaInhoud::where('pagina', '=', 'pagina_6')->get()->first());
    }


    public function update_1(Request $request){
      $cookie = Cookie::get('gestemd_1');
      if(!$cookie){
        // Als cookie niet aanwezig is worden de waarden in de database aangepast bij het klikken op een van de duimpjes
        if($request->has('ja')){
          PaginaLikes::where('pagina', '=', 'pagina_1')->increment('totaal');
          PaginaLikes::where('pagina', '=', 'pagina_1')->increment('nuttig');
        }
        if($request->has('nee')){
          PaginaLikes::where('pagina', '=', 'pagina_1')->increment('totaal');
        }
        // Hier wordt de cookie aangemaakt zodat de gebruiker niet nog een keer kan stemmen
        $response = new \Illuminate\Http\Response(view('pagina.pagina_like')->with('likes',PaginaLikes::where('pagina', '=', 'pagina_1')->get()->first()));
        $response->withCookie(cookie('gestemd_1', 'true', 1000));
        return $response;
      } else {
        // Als de cookie wel aanwezig is kan de persoon in kwestie niet meer stemmen en wordt er een message meegegeven
        return Redirect::to('/pagina_1')->with('message', 'U heeft uw mening al gegeven!');
      }
    }

    public function update_2(Request $request){
      $cookie = Cookie::get('gestemd_2');
      if(!$cookie){
        if($request->has('ja')){
          PaginaLikes::where('pagina', '=', 'pagina_2')->increment('totaal');
          PaginaLikes::where('pagina', '=', 'pagina_2')->increment('nuttig');
        }
        if($request->has('nee')){
          PaginaLikes::where('pagina', '=', 'pagina_2')->increment('totaal');
        }
        $response = new \Illuminate\Http\Response(view('pagina.pagina_like')->with('likes',PaginaLikes::where('pagina', '=', 'pagina_2')->get()->first()));
        $response->withCookie(cookie('gestemd_2', 'true', 1000));
        return $response;
      } else {
        return Redirect::to('/pagina_2')->with('message', 'U heeft uw mening al gegeven!');
      }
    }

    public function update_3(Request $request){
      $cookie = Cookie::get('gestemd_3');
      if(!$cookie){
        if($request->has('ja')){
          PaginaLikes::where('pagina', '=', 'pagina_3')->increment('totaal');
          PaginaLikes::where('pagina', '=', 'pagina_3')->increment('nuttig');
        }
        if($request->has('nee')){
          PaginaLikes::where('pagina', '=', 'pagina_3')->increment('totaal');
        }
        $response = new \Illuminate\Http\Response(view('pagina.pagina_like')->with('likes',PaginaLikes::where('pagina', '=', 'pagina_3')->get()->first()));
        $response->withCookie(cookie('gestemd_3', 'true', 1000));
        return $response;
      } else {
        return Redirect::to('/pagina_3')->with('message', 'U heeft uw mening al gegeven!');
      }
    }

    public function update_4(Request $request){
      $cookie = Cookie::get('gestemd_4');
      if(!$cookie){
        if($request->has('ja')){
          PaginaLikes::where('pagina', '=', 'pagina_4')->increment('totaal');
          PaginaLikes::where('pagina', '=', 'pagina_4')->increment('nuttig');
        }
        if($request->has('nee')){
          PaginaLikes::where('pagina', '=', 'pagina_4')->increment('totaal');
        }
        $response = new \Illuminate\Http\Response(view('pagina.pagina_like')->with('likes',PaginaLikes::where('pagina', '=', 'pagina_4')->get()->first()));
        $response->withCookie(cookie('gestemd_4', 'true', 1000));
        return $response;
      } else {
        return Redirect::to('/pagina_4')->with('message', 'U heeft uw mening al gegeven!');
      }
    }

    public function update_5(Request $request){
      $cookie = Cookie::get('gestemd_5');
      if(!$cookie){
        if($request->has('ja')){
          PaginaLikes::where('pagina', '=', 'pagina_5')->increment('totaal');
          PaginaLikes::where('pagina', '=', 'pagina_5')->increment('nuttig');
        }
        if($request->has('nee')){
          PaginaLikes::where('pagina', '=', 'pagina_5')->increment('totaal');
        }
        $response = new \Illuminate\Http\Response(view('pagina.pagina_like')->with('likes',PaginaLikes::where('pagina', '=', 'pagina_5')->get()->first()));
        $response->withCookie(cookie('gestemd_5', 'true', 1000));
        return $response;
      } else {
        return Redirect::to('/pagina_5')->with('message', 'U heeft uw mening al gegeven!');
      }
    }

    public function update_6(Request $request){
      $cookie = Cookie::get('gestemd_6');
      if(!$cookie){
        if($request->has('ja')){
          PaginaLikes::where('pagina', '=', 'pagina_6')->increment('totaal');
          PaginaLikes::where('pagina', '=', 'pagina_6')->increment('nuttig');
        }
        if($request->has('nee')){
          PaginaLikes::where('pagina', '=', 'pagina_6')->increment('totaal');
        }
        $response = new \Illuminate\Http\Response(view('pagina.pagina_like')->with('likes',PaginaLikes::where('pagina', '=', 'pagina_6')->get()->first()));
        $response->withCookie(cookie('gestemd_6', 'true', 1000));
        return $response;
      } else {
        return Redirect::to('/pagina_6')->with('message', 'U heeft uw mening al gegeven!');
      }
    }
}
