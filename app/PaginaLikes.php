<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaginaLikes extends Model
{
    protected $table = 'likes';
    public $timestamps = false;
}
