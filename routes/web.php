<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/pagina_1','PaginaController@pagina_1');
Route::get('/pagina_2','PaginaController@pagina_2');
Route::get('/pagina_3','PaginaController@pagina_3');
Route::get('/pagina_4','PaginaController@pagina_4');
Route::get('/pagina_5','PaginaController@pagina_5');
Route::get('/pagina_6','PaginaController@pagina_6');
Route::post('/pagina_1','PaginaController@update_1');
Route::post('/pagina_2','PaginaController@update_2');
Route::post('/pagina_3','PaginaController@update_3');
Route::post('/pagina_4','PaginaController@update_4');
Route::post('/pagina_5','PaginaController@update_5');
Route::post('/pagina_6','PaginaController@update_6');

Route::post('/dashboard','DashboardController@dashboard_upload')->middleware(['auth','admin']);
Route::get('/dashboard','DashboardController@dashboard')->middleware(['auth','admin']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
